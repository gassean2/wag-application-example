// API 3
var express = require('express');
var router = express.Router();

// routes
// GET, get the collection
router.get('/service', function (req, res) {
    var responseObject = '{"person": ""}';

    res.send(responseObject);
});

// GET, get a single entry.
router.get('/service/:id', function (req, res) {
    // get ID
    var id = req.params.id;
    res.send('{"workstationId": "' + id + '","messages":[]}');
});

// PUT, updates or creates a single entry
router.put('/service/:id', function (req, res) {
    // get ID
    var id = req.params.id;
    var responseObject = '{"person": "' + id + '"}';

    res.send(responseObject);
});

// DELETE, delete the collection
router.delete('/service', function (req, res) {
    var responseObject = '{"person": ""}';

    res.send(responseObject);
});

// DELETE, delete an entry
router.delete('/service/:id', function (req, res) {
    // get ID
    var id = req.params.id;
    var responseObject = '{"person": "' + id + '"}';

    res.send(responseObject);
});

module.exports = router;
