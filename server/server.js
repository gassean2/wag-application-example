var express = require('express'); // http server
var jsonServer = require('json-server'); // json-server incl CRUD API
var pause = require('connect-pause'); // delay service response
var server;

// import all APIs
var api1 = jsonServer.router('./db.json'); // invoke the json-server api for simple CRUD operations against the db.json in memory
var api2 = require('./api-2');
var api3 = require('./api-3');

// setup server
var app = express();

// Delay all responses incl file serving in ms, must come before routes.
app.use(pause(100));

/*
// Middleware: must come before routes
app.use(function (req, res, next) {
    // modifed response here
    next();
});
*/

// Routes: add routes with prefixes
// json-server API
app.use('/api1/v1', jsonServer.defaults()); // Optional, if you want to have JSON Server defaults like CORS.
app.use('/api1/v1', api1);
// other APIs
app.use('/api2/v1', api2);
app.use('/api3/v1', api3);

// expose the folder with static files
// app.use(express.static('dist'));

// redirect root calls to index.html
// app.get('/', function (req, res) {
//     res.sendFile(__dirname + '/' + 'index.html');
// });

// start server and expose all services on specific port.
// the port must match the proxy in the 'serve' task in gulpfile.js
server = app.listen(3030, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Server app listening at http://' + host + ':' + port);
});
