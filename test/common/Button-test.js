/* global describe, it, beforeEach */

import React from 'react';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import { spy } from 'sinon';

import Button from './../../src/js/common/Button';

describe('Button Testing Suite', () => {
    let onButtonClickedSpy;

    beforeEach(() => {
        onButtonClickedSpy = spy();
    });

    it('should render correctly', () => {
        const output = shallow(<Button
          onButtonClicked={onButtonClickedSpy}
        />);

        expect(output).to.be.ok;
        expect(output.props().className).to.equal('btn btn-primary');
    });

    it('should fire the click action', () => {
        const output = mount(<Button
          onButtonClicked={onButtonClickedSpy}
        />);
        output.simulate('click');
        expect(onButtonClickedSpy.args.length).to.equal(1);

        const randomlyGeneratedNumber = onButtonClickedSpy.args[0][0];

        expect(randomlyGeneratedNumber).to.be.at.most(100);
        expect(randomlyGeneratedNumber).to.be.at.least(0);
    });
});
