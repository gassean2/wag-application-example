/* global describe, it */

import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import Alert from 'react-bootstrap/lib/Alert';

import Message from './../../src/js/common/Message';

describe('Message Testing Suite', () => {
    it('should render an Alert', () => {
        const text = 'sample text';
        const output = shallow(<Message text={text} />);

        const alert = output.find(Alert);
        expect(alert.props().children.props.children).to.equal(`"${text}"`);
    });
});

