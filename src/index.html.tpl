<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <script>
            if (document.location.hostname === 'localhost') document.location.hostname = 'localhost.na.novartis.net';
        </script>
        <title>WAE</title>

        <link rel="stylesheet" href="//nebulacdn.na.novartis.net/component/nibr-bootstrap/4.0.0/dist/css/bootstrap.min.css">
        <link rel="stylesheet icon" href="assets/bootstrap-grid.min.css">
        <link rel="shortcut icon" href="assets/favicon.ico">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <section id="content" class="application"></section>
        <script src="js/<%= bundleFile %>"></script>
    </body>
</html>
