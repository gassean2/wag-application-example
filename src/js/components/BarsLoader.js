import React from 'react';

const BarsLoader = () => (<ul className="BarsLoader__loader">
    <li /><li /><li />
</ul>);

export default BarsLoader;
