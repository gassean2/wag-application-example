import React from 'react';
import PropTypes from 'prop-types';

import BarsLoader from '../components/BarsLoader';

const FetchingButton = (props) => {
    const { fetching, value, onClick, disabled } = props;
    return (
        <button
          className="btn btn-default"
          onClick={() => {
              if (!fetching) {
                  onClick();
              }
          }}
          disabled={disabled || fetching}
        >{fetching ? <BarsLoader /> : value}</button>
    );
};

FetchingButton.propTypes = {
    fetching: PropTypes.bool.isRequired,
    value: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
};

FetchingButton.defaultProps = {
    disabled: false,
};

export default FetchingButton;
