import PropTypes from 'prop-types';

const personObjectPropType = PropTypes.shape({
    academic_title: PropTypes.string,
    comments: PropTypes.string,
    cost_center: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    country: PropTypes.string,
    country_code: PropTypes.string,
    department: PropTypes.string,
    division: PropTypes.string,
    employee_id: PropTypes.string,
    employee_type: PropTypes.string,
    firstname: PropTypes.string,
    functionalTitle: PropTypes.string,
    glf: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    internal_address: PropTypes.string,
    lastname: PropTypes.string,
    mail: PropTypes.string,
    phone: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    referenceNo: PropTypes.string,
    sector: PropTypes.string,
    site_code: PropTypes.string,
    site_name: PropTypes.string,
    state: PropTypes.string,
    sub_department: PropTypes.string,
    sub_function: PropTypes.string,
    sub_function_group: PropTypes.string,
    unique_id: PropTypes.string,
    work_address: PropTypes.string,
});

export {
    personObjectPropType,
};

export default personObjectPropType;
