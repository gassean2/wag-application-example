import serviceEndpoints from '../../../service_endpoints.json';

const getEnv = () => {
    const hostname = window.location.hostname;
    let env = '';
    if (hostname.indexOf('web.global') === 0) {
        env = 'prod';
    } else if (hostname.indexOf('web-test.global') === 0) {
        env = 'test';
    } else if (hostname.indexOf('web-dev.global') === 0) {
        env = 'dev';
    } else {
        env = 'local';
    }
    return env;
};

const getURL = service => serviceEndpoints[service][getEnv()];

export {
    getURL,
    getEnv,
};
