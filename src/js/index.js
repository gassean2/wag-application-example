import { Router } from 'react-router';
import { Provider } from 'react-redux';

import React from 'react';
import ReactDOM from 'react-dom';
import createStoreAndHistory from './redux/config/store';
import routes from './routes/routes';
import DevTools from './pages/main/DevTools';

require('es6-promise').polyfill();
require('isomorphic-fetch');

const { store, history } = createStoreAndHistory();

ReactDOM.render(
    <Provider store={store}>
        <div>
            <Router history={history}>
                { routes }
            </Router>
            <DevTools />
        </div>
    </Provider>,
    document.getElementById('content')
);
