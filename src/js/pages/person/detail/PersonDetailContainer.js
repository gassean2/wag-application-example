import { connect } from 'react-redux';

import { createPerson, updatePerson } from '../../../redux/person/actions';
import PersonDetailView from './PersonDetailView';

const mapStateToProps = (state) => {
    const {
        peopleById,
        activePersonId,
        network,
    } = state.person;

    const person = peopleById[activePersonId];

    return {
        person,
        network,
    };
};

const mapDispatchToProps = ({
    createPerson,
    updatePerson,
});

const PersonDetailContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(PersonDetailView);

export default PersonDetailContainer;
