import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { personObjectPropType } from '../../../common/customPropTypes';
import FetchingButton from '../../../common/FetchingButton';

import * as CrudKeys from '../../../enums/CrudKeys';
import * as RequestStatus from '../../../enums/RequestStatus';

const initialState = {
    comments: '',
    editing: false,
};

class PersonDetailView extends Component {
    static propTypes = {
        person: personObjectPropType,
        createPerson: PropTypes.func.isRequired,
        updatePerson: PropTypes.func.isRequired,
        network: PropTypes.shape({}).isRequired,
    };

    static defaultProps = {
        person: {},
    };

    constructor(props) {
        super(props);

        this.state = initialState;

        this.handleChange = this.handleChange.bind(this);
        this.onAddPersonClick = this.onAddPersonClick.bind(this);
        this.onUpdatePersonClick = this.onUpdatePersonClick.bind(this);
        this.onCancelUpdatePersonClick = this.onCancelUpdatePersonClick.bind(this);
    }

    componentWillMount() {
        this.resetState(this.props);
    }

    componentWillReceiveProps(newProps) {
        this.resetState(newProps);
    }

    resetState(props) {
        this.setState({
            comments: props.person.comments || '',
            editing: false,
        });
    }

    onAddPersonClick() {
        const { person, createPerson } = this.props;
        createPerson(person);
    }

    onUpdatePersonClick() {
        const { comments } = this.state;
        const { person, updatePerson } = this.props;
        // const newPerson = Object.assign({}, person, { comments });
        const newPerson = { ...person, comments };
        updatePerson(newPerson);
    }

    onCancelUpdatePersonClick() {
        this.resetState(this.props);
    }

    /**
     *   using high-order function to handle onChange event for multiple input events.
     *   key specifies the state key to set input value
     **/
    handleChange(key) {
        return (e) => {
            const newState = {
                editing: true,
            };
            newState[key] = e.target.value;
            this.setState(newState);
        };
    }

    renderButtons() {
        const { editing } = this.state;
        const { person, network } = this.props;
        const className = 'person-actions';
        if (!person.id) {
            return (
                <div className={className}>
                    <FetchingButton
                      onClick={this.onAddPersonClick}
                      value="Add to list"
                      fetching={network[CrudKeys.CREATE].status === RequestStatus.PENDING}
                    />
                </div>
            );
        }

        return (
            <div className={className}>
                <FetchingButton
                  onClick={this.onUpdatePersonClick}
                  value="Update"
                  fetching={network[CrudKeys.UPDATE].status === RequestStatus.PENDING}
                  disabled={!editing}
                />
                <button
                  className="btn btn-default"
                  onClick={this.onCancelUpdatePersonClick}
                  disabled={!editing}
                >Cancel</button>
            </div>
        );
    }

    renderCommentsSection(comments) {
        return (
            <div>
                <h4>Comments</h4>
                <div className="form-group">
                    <textarea
                      value={comments}
                      onChange={this.handleChange('comments')}
                      className="form-control"
                      rows="3"
                      placeholder="Please add comments..."
                    />
                </div>
            </div>
        );
    }

    render() {
        const { comments } = this.state;
        const { person } = this.props;

        if (!person) {
            return <div>Nothing to see here... </div>;
        }

        return (
            <div>
                {this.renderButtons()}
                <pre>
                    <ul className="person-details-list">
                        { Object.keys(person).filter(k => k !== 'comments').map((key, i) => (
                            <li key={i}>
                                <span>{key}</span>{person[key]}
                            </li>
                        ))
                        }
                    </ul>
                </pre>
                { person.id && this.renderCommentsSection(comments) }
            </div>
        );
    }
}

export default PersonDetailView;
