import { connect } from 'react-redux';
import { setVisibilityFilter } from '../../../redux/person/actions';
import Link from '../../../components/FilterLink';

const mapStateToProps = (state, ownProps) => (
    {
        active: ownProps.filter === state.person.visibilityFilter,
    }
);

const mapDispatchToProps = (dispatch, ownProps) => ({
    onClick: () => dispatch(setVisibilityFilter(ownProps.filter)),
});

const PersonFilterContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Link);

export default PersonFilterContainer;
