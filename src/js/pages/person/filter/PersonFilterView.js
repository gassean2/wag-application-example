import React from 'react';

import * as PersonFilterTypes from '../../../enums/PersonFilterTypes';
import PersonFilterItemContainer from './PersonFilterItemContainer';

const PersonFilter = () => (
  <p className="filter">
    Show:
    {' '}
    <PersonFilterItemContainer filter={PersonFilterTypes.SHOW_ALL}>
      All
    </PersonFilterItemContainer>
    {', '}
    <PersonFilterItemContainer filter={PersonFilterTypes.SHOW_INTERNAL}>
      Internal
    </PersonFilterItemContainer>
    {', '}
    <PersonFilterItemContainer filter={PersonFilterTypes.SHOW_EXTERNAL}>
      External
    </PersonFilterItemContainer>
  </p>
);

export default PersonFilter;
