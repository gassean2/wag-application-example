import React from 'react';
import PropTypes from 'prop-types';

const PersonListItem = ({ id, peopleById, onClick, onIconClick }) => {
    const person = peopleById[id];
    return (
        <li>
            <a
              key={id}
              onClick={() => onClick(person)}
              tabIndex="0"
            >
                <span>{person.firstname} {person.lastname}</span>
                <i
                  className="fa fa-trash-o"
                  aria-hidden="true"
                  onClick={(e) => onIconClick(person, e)}
                />
            </a>
        </li>
    );
};

PersonListItem.propTypes = {
    id: PropTypes.number.isRequired,
    peopleById: PropTypes.shape({}).isRequired,
    onClick: PropTypes.func.isRequired,
    onIconClick: PropTypes.func.isRequired,
};

export default PersonListItem;
