import React from 'react';
import PropTypes from 'prop-types';

import * as RequestStatus from '../../../enums/RequestStatus';

import BarsLoader from '../../../components/BarsLoader';
import PersonListItem from './PersonListItem';

const PersonListView = ({
    network,
    peopleById,
    visiblePeopleIds,
    setActivePerson,
    deletePerson,
}) => {
    const { GET } = network;

    if (!GET || GET.status === RequestStatus.PENDING) {
        return <BarsLoader />;
    }

    if (GET.status === RequestStatus.FAILURE) {
        return null;
    }

    return (
        <ul className="person-list">
            {visiblePeopleIds.map((id) => (
                <PersonListItem
                  key={id}
                  id={id}
                  peopleById={peopleById}
                  onClick={setActivePerson}
                  onIconClick={deletePerson}
                />
            ))}
        </ul>
    );
};

PersonListView.propTypes = {
    network: PropTypes.shape({}).isRequired,
    peopleById: PropTypes.shape({}).isRequired,
    visiblePeopleIds: PropTypes.arrayOf(PropTypes.number).isRequired,
    setActivePerson: PropTypes.func.isRequired,
    deletePerson: PropTypes.func.isRequired,
};

export default PersonListView;
