import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PersonListContainer from './person-list/PersonListContainer';
import PersonDetailContainer from './detail/PersonDetailContainer';
import PersonFilter from './filter/PersonFilterView';
import PeopleSuggest from './PeopleSuggest';

class PersonView extends Component {
    static propTypes = {
        getPeople: PropTypes.func.isRequired,
        setActivePerson: PropTypes.func.isRequired,
        dispatchErrorNotification: PropTypes.func.isRequired,
        dispatchFetchErrorNotification: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        this.onPeopleSuggestSubmit = this.onPeopleSuggestSubmit.bind(this);
    }

    componentWillMount() {
        this.props.getPeople();
    }

    onPeopleSuggestSubmit(person) {
        this.props.setActivePerson(person);
    }

    render() {
        const {
            dispatchErrorNotification,
            dispatchFetchErrorNotification,
        } = this.props;
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col">
                        <h1>Person List</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-4">
                        <h3>Search Person</h3>
                        <PeopleSuggest
                          onSubmit={this.onPeopleSuggestSubmit}
                          onError={dispatchErrorNotification}
                          onFetchError={dispatchFetchErrorNotification}
                        />
                        <h3>Contact list</h3>
                        <PersonFilter />
                        <PersonListContainer />
                    </div>
                    <div className="col-8">
                        <h3>Show Person Detail</h3>
                        <PersonDetailContainer />
                    </div>
                </div>
            </div>
        );
    }
}

export default PersonView;
