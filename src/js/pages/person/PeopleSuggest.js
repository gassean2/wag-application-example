import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NxSearchField from 'nxc-core/NxSearchField';
import { debounce } from 'throttle-debounce';

import { getURL } from '../../common/fetchUtils';

const initialState = {
    suggestions: [],
    fetchCounter: 0,
    selectedPerson: {},
    value: '',
};

class PeopleSuggest extends Component {

    static propTypes = {
        onSubmit: PropTypes.func.isRequired,
        onError: PropTypes.func.isRequired,
        onFetchError: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = initialState;

        this.fetchSuggestions = debounce(300, this.fetchSuggestions); // add delay

        this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
        this.handleSuggestions = this.handleSuggestions.bind(this);
        this.handleSuggestionsClearRequest = this.handleSuggestionsClearRequest.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleValueChange = this.handleValueChange.bind(this);
    }

    onSuggestionSelected(e, { suggestionValue }) {
        this.setState({
            selectedPerson: suggestionValue,
        });

        this.props.onSubmit(suggestionValue);
    }

    onSubmit() {
        this.props.onSubmit(this.state.selectedPerson);
    }

    handleValueChange(e, { method, newValue }) {
        if (method !== 'type' && method !== 'enter') {
            return;
        }
        this.setState({ value: newValue });
    }

    handleSuggestions({ value }) {
        if (value.length < 3) return;

        if (value.length < 0) {
            this.setState({
                suggestions: [],
                selectedPerson: {},
                value: '',
            });
        }

        this.fetchSuggestions(value);
    }

    handleSuggestionsClearRequest() {
        this.setState({ suggestions: [] });
    }

    fetchSuggestions(value) {
        const { onError, onFetchError } = this.props;

        // increase fetch counter
        const fetchId = new Date().getTime();
        this.setState({
            fetchId,
        });

        const param = String(value).toLowerCase();
        const url = `${getURL('serviceBaseUrl')}/ps/v1/people/search/${param}`;

        fetch(url, {
            method: 'GET',
            mode: 'cors',
            credentials: 'include',
        }).then((res) => {
            if (res.ok) {
                res.json().then((json) => {
                    // only set state for last fetch response
                    if (this.state.fetchId === fetchId) {
                        this.setState({
                            suggestions: json.results,
                        });
                    }
                });
            } else {
                onFetchError(res);
            }
        })
        .catch((err) => {
            onError(err);
        });
    }

    renderSuggestion(person) {
        return <span>{person.firstname} {person.lastname} ({person.unique_id})</span>;
    }

    render() {
        const { suggestions, value } = this.state;
        const inputProps = {
            placeholder: 'Enter User521...',
            value,
            onChange: this.handleValueChange,
        };
        return (
            <NxSearchField
              id="1"
              suggestions={suggestions}
              getSuggestionValue={person => person}
              renderSuggestion={this.renderSuggestion}
              onSuggestionSelected={this.onSuggestionSelected}
              onSuggestionsFetchRequested={this.handleSuggestions}
              onSuggestionsClearRequested={this.handleSuggestionsClearRequest}
              onSubmit={this.onSubmit}
              inputProps={inputProps}
            />
        );
    }
}

export default PeopleSuggest;
