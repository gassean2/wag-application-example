import { connect } from 'react-redux';

import PersonView from './PersonView';

import {
    getPeople,
    setActivePerson,
} from '../../redux/person/actions';

import {
    dispatchErrorNotification,
    dispatchFetchErrorNotification,
} from '../../redux/notifications/actions';

const mapStateToProps = () => ({});

const mapDispatchToProps = ({
    getPeople,
    setActivePerson,
    dispatchErrorNotification,
    dispatchFetchErrorNotification,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PersonView);
