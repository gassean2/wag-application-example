import React from 'react';

const Footer = () => {
    const styles = {
        root: {
            width: '100vw',
            height: '35px',
            borderTop: '1px solid #EDEDED',
            display: 'flex',
            padding: '1em',
            alignItems: 'center',
            fontSize: '10px',
            color: '#C6C6C6',
            backgroundColor: 'white',
            marginTop: '20px',
        },
        anchor: {
            paddingLeft: '5px',
        },
    };

    const href = 'http://repo.nibr.novartis.net/artifactory/nibr-web/wag/docs/release/0.3.0/index.html';

    return (<div style={styles.root}>
        <span>Generated on 6/30/2017, 6:47:03 AM with WAG version:</span>
        <a
          style={styles.anchor}
          href={href}
          target="_blank"
        ><code>0.3.0</code></a>
    </div>);
};

export default Footer;
