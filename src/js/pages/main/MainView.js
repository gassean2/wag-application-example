import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NxHeader from 'nxc-core/NxHeader';
import NxGetHelp from 'nxc-core/NxGetHelp';
import Notifications from 'react-notification-system-redux';

import Footer from './Footer';

import * as Paths from '../../enums/Paths';

class MainView extends Component {

    static propTypes = {
        children: PropTypes.node,
        notifications: PropTypes.arrayOf(PropTypes.object).isRequired,
    };

    static defaultProps = {
        children: '',
    };

    constructor(props) {
        super(props);
        this.state = {
            person: '',
        };
    }

    handleAuth(person) {
        this.setState({ person });
    }

    render() {
        const { notifications } = this.props;

        const appName = 'WAE';
        const activePath = window.location.hash;
        const barLinks = [
            {
                label: 'Home',
                path: `#${Paths.INDEX}`,
            },
            {
                label: 'Person List',
                path: `#${Paths.PERSON}`,
            },
            {
                label: 'Notifications',
                path: `#${Paths.NOTIFICATIONS}`,
            },
            {
                label: 'Routing Fail Example',
                path: `#${Paths.FAIL}`,
            },
        ];

        const jiraOptions = {
            issueTypeName: 'Bug',
            defaultAssignee: 'gassean2',
            projectCode: 'JTP',
            component: 'Feedback/Suggestion',
        };

        const appLinks = [
            {
                url: 'http://go/ui',
                name: 'FAQs',
            },
            {
                url: 'http://go/ui',
                name: 'Documentation',
            },
            {
                url: 'http://go/ui',
                name: 'NIBRTube Tutorials',
            },
            {
                url: 'http://go/ui',
                name: 'Contact Test App Team',
            },
        ];

        const helpContent = (
            <NxGetHelp
              appName={appName}
              appLinks={appLinks}
              deployState="dev"
              jiraOptions={jiraOptions}
            />
        );

        return (
            <div className="application-wrapper">
                <div className="content">
                    <div style={{ paddingBottom: '1rem' }}>
                        <NxHeader
                          appName={appName}
                          description={'wag-application-example'}
                          deployState={'dev'}
                          titleHref={'/'}
                          onTitleClick={null}
                          navBarInfo={{ activePath, barLinks }}
                          helpTrayContent={helpContent}
                        />
                    </div>
                    <Notifications
                      notifications={notifications}
                    />
                    { this.props.children }
                </div>
                <Footer />
            </div>
        );
    }
}

export default MainView;
