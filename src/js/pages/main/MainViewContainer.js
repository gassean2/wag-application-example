import { connect } from 'react-redux';

import MainView from './MainView';

import {
    routeToPath,
} from '../../routes/actions';

// connect notification state to MainView to dispatch error notifications
const mapStateToProps = state => ({ notifications: state.notifications });

const mapDispatchToProps = ({
    routeToPath,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(MainView);
