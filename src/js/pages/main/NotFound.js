import React from 'react';

const NotFound = () => (
    <div className="jumbotron">
        <h1>Not found</h1>
        <h2>No route for <code>{document.location.hash}</code></h2>
    </div>
);

export default NotFound;
