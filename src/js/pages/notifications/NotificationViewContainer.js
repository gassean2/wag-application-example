import { connect } from 'react-redux';

import NotificationView from './NotificationView';

import {
    dispatchFetchErrorNotification,
    dispatchErrorNotification,
    dispatchSuccessNotification,
    dispatchInfoNotification,
    dispatchWarningNotification,
} from '../../redux/notifications/actions';

// connect notification state to MainView to dispatch error notifications
const mapStateToProps = state => ({ notifications: state.notifications });

const mapDispatchToProps = ({
    dispatchFetchErrorNotification,
    dispatchErrorNotification,
    dispatchSuccessNotification,
    dispatchInfoNotification,
    dispatchWarningNotification,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(NotificationView);
