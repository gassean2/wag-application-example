import React, { Component } from 'react';
import PropTypes from 'prop-types';

class AboutView extends Component {
    static propTypes = {
        notifications: PropTypes.arrayOf(PropTypes.object).isRequired,
        dispatchErrorNotification: PropTypes.func.isRequired,
        dispatchFetchErrorNotification: PropTypes.func.isRequired,
        dispatchSuccessNotification: PropTypes.func.isRequired,
        dispatchInfoNotification: PropTypes.func.isRequired,
        dispatchWarningNotification: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        this.triggerNotifications = this.triggerNotifications.bind(this);
    }

    triggerNotifications() {
        const {
            dispatchErrorNotification,
            dispatchFetchErrorNotification,
            dispatchSuccessNotification,
            dispatchInfoNotification,
            dispatchWarningNotification,
        } = this.props;

        dispatchErrorNotification({ message: 'Error message' });
        dispatchFetchErrorNotification({
            message: 'Fetch Error message',
            status: 400,
            statusText: 'Not found',
        });

        dispatchSuccessNotification({ title: 'Success', message: 'success message' });
        dispatchInfoNotification({ title: 'Info', message: 'info message' });
        dispatchWarningNotification({ title: 'Warning', message: 'warning message' });
    }

    render() {
        const { notifications } = this.props;

        const listItems = notifications.map((notification, i) => (
            <li key={i}>{JSON.stringify(notification)}</li>
        ));

        return (
            <div className="container-fluid">
                <h1>Notification</h1>
                <p>Trigger notifications with click on the button below</p>

                <button
                  className="btn btn-primary"
                  onClick={this.triggerNotifications}
                >Trigger Notifications</button>

                <br /><br />
                <strong>All notifications</strong>
                <ul>{listItems}</ul>
            </div>
        );
    }
}

export default AboutView;
