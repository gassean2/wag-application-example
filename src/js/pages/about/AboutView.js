import React from 'react';

const topics = [
    'nxc-core',
    'header with navigation and GetHelp',
    'NIBR footer',
    'CRUD implementation (Create, Read, Update Delete)',
    'handling fetching state example',
    'error handling for CRUD',
    'centralized notification system',
    'custom routing example (routes called programmatically)',
    'Redux DevTools',
    'people suggest',
];

const AboutView = () => (
    <div className="jumbotron">
        <h1>Welcome to the WAG application example</h1>
        <p>This example is based on WAG and includes almost everything
            you will need for next NIBR application.</p>

        <strong>Included:</strong>
        <ul>
            {topics.map((topic, i) => <li key={i}>{topic}</li>)}
        </ul>
    </div>
);

export default AboutView;
