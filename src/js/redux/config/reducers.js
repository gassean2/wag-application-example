import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import notificationsReducer from '../notifications/reducer';
import personReducer from '../person/reducer';

export default combineReducers({
    routing: routerReducer,
    notifications: notificationsReducer,
    person: personReducer,
});
