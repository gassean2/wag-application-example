
/*
    Creates object with key, type and payload.
    Type and payload is used for data reducers to handle any data changes / fetches
    key is only used for handling async requests and to track status of those requests

    disabled eslint arrow-body-style rul for for better readability
 */
const createAsyncAction = (key, type) => { // eslint-disable-line arrow-body-style
    return (payload) => ({
        key,
        type,
        payload,
    });
};

export {
    createAsyncAction,
};

export default createAsyncAction;
