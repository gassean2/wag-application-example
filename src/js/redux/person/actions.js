import { getURL } from '../../common/fetchUtils';

import * as Consts from './consts';
import * as CrudKeys from '../../enums/CrudKeys';

import {
    dispatchErrorNotification,
    dispatchFetchErrorNotification,
} from '../notifications/actions';

const personUrl = getURL('personUrl');

const setActivePerson = (activePerson) => ({
    type: Consts.SET_ACTIVE_PERSON,
    payload: activePerson,
});
const setVisibilityFilter = (filter) => ({
    type: Consts.SET_VISIBILITY_FILTER,
    payload: filter,
});

const getPeopleRequest = () => ({
    type: Consts.GET_PEOPLE_REQUEST,
    key: CrudKeys.GET,
});

const getPeopleSuccess = (data) => {
    const peopleById = {};
    const peopleIds = [];

    // proceed persons array
    data.forEach((people) => {
        peopleById[people.id] = people;
        peopleIds.push(people.id);
    });

    const payload = {
        peopleById,
        peopleIds,
    };

    return {
        type: Consts.GET_PEOPLE_SUCCESS,
        key: CrudKeys.GET,
        payload,
    };
};

const getPeopleFailure = (error) => ({
    type: Consts.GET_PEOPLE_FAILURE,
    key: CrudKeys.GET,
    payload: error,
});

const createPersonRequest = () => ({
    type: Consts.CREATE_PERSON_REQUEST,
    key: CrudKeys.CREATE,
});

const createPersonSuccess = (person) => ({
    type: Consts.CREATE_PERSON_SUCCESS,
    key: CrudKeys.CREATE,
    payload: person,
});

const createPersonFailure = (error) => ({
    type: Consts.CREATE_PERSON_FAILURE,
    key: CrudKeys.CREATE,
    payload: error,
});

const updatePersonRequest = () => ({
    type: Consts.UPDATE_PERSON_REQUEST,
    key: CrudKeys.UPDATE,
});

const updatePersonSuccess = (person) => ({
    type: Consts.UPDATE_PERSON_SUCCESS,
    key: CrudKeys.UPDATE,
    payload: person,
});

const updatePersonFailure = (error) => ({
    type: Consts.UPDATE_PERSON_FAILURE,
    key: CrudKeys.UPDATE,
    payload: error,
});

const deletePersonRequest = () => ({
    type: Consts.DELETE_PERSON_REQUEST,
    key: CrudKeys.DELETE,
});

const deletePersonSuccess = (person) => ({
    type: Consts.DELETE_PERSON_SUCCESS,
    key: CrudKeys.DELETE,
    payload: person,
});

const deletePersonFailure = (error) => ({
    type: Consts.DELETE_PERSON_FAILURE,
    key: CrudKeys.DELETE,
    payload: error,
});

// complex actions
const getPeople = () => {
    const url = personUrl;
    return (dispatch) => {
        dispatch(getPeopleRequest());
        return fetch(url, {
            method: 'GET',
            mode: 'cors',
            credentials: 'include',
        })
            .then((res) => {
                if (res.ok) {
                    res.json().then((json) => {
                        dispatch(getPeopleSuccess(json));
                    });
                } else {
                    dispatch(getPeopleFailure());
                    dispatch(dispatchFetchErrorNotification(res));
                }
            })
            .catch((err) => {
                dispatch(getPeopleFailure());
                dispatch(dispatchErrorNotification(err));
            });
    };
};

const createPerson = (person) => {
    const url = personUrl;
    return (dispatch) => {
        dispatch(createPersonRequest());
        return fetch(url, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(person),
        })
            .then((res) => {
                if (res.ok) {
                    res.json().then((json) => {
                        dispatch(createPersonSuccess(json));
                    });
                } else {
                    dispatch(createPersonFailure());
                    dispatch(dispatchFetchErrorNotification(res));
                }
            })
            .catch((err) => {
                dispatch(createPersonFailure(err));
                dispatch(dispatchErrorNotification(err));
            });
    };
};

const updatePerson = (person) => {
    const url = `${personUrl}/${person.id}`;
    return (dispatch) => {
        dispatch(updatePersonRequest());
        return fetch(url, {
            method: 'PUT',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(person),
        })
            .then((res) => {
                if (res.ok) {
                    res.json().then((json) => {
                        dispatch(updatePersonSuccess(json));
                    });
                } else {
                    dispatch(updatePersonFailure());
                    dispatch(dispatchFetchErrorNotification(res));
                }
            })
            .catch((err) => {
                dispatch(updatePersonFailure(err));
                dispatch(dispatchErrorNotification(err));
            });
    };
};

const deletePerson = (person) => {
    const url = `${personUrl}/${person.id}`;
    return (dispatch) => {
        dispatch(deletePersonRequest());
        return fetch(url, {
            method: 'DELETE',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(person),
        })
            .then((res) => {
                if (res.ok) {
                    dispatch(deletePersonSuccess(person));
                } else {
                    dispatch(deletePersonFailure());
                    dispatch(dispatchFetchErrorNotification(person));
                }
            })
            .catch((err) => {
                dispatch(deletePersonFailure(err));
                dispatch(dispatchErrorNotification(err));
            });
    };
};

export {
    setActivePerson,
    setVisibilityFilter,
    getPeople,
    createPerson,
    updatePerson,
    deletePerson,
};
