import * as Consts from './consts';

import * as RequestStatus from '../../enums/RequestStatus';
import * as CrudKeys from '../../enums/CrudKeys';
import * as PersonFilterTypes from '../../enums/PersonFilterTypes';

const peopleById = (state = {}, action) => {
    let newState = { ...state };
    const { type, payload } = action;
    switch (type) {
        case Consts.GET_PEOPLE_SUCCESS:
            return payload.peopleById;
        case Consts.UPDATE_PERSON_SUCCESS:
        case Consts.CREATE_PERSON_SUCCESS:
            newState = {
                ...state,
            };
            newState[payload.id] = payload;
            return newState;
        case Consts.DELETE_PERSON_SUCCESS:
            newState = {
                ...state,
            };
            delete newState[payload.id];
            return newState;
        case Consts.SET_ACTIVE_PERSON:
            newState = {
                ...state,
            };
            newState[0] = payload;
            return newState;
        default:
            return state;
    }
};

const peopleIds = (state = [], action) => {
    const { type, payload } = action;
    let newArray;
    switch (type) {
        case Consts.GET_PEOPLE_SUCCESS:
            return payload.peopleIds;
        case Consts.CREATE_PERSON_SUCCESS:
            return [...state, payload.id];
        case Consts.DELETE_PERSON_SUCCESS:
            newArray = [...state];
            newArray.splice(newArray.indexOf(payload.id), 1);
            return newArray;
        default:
            return state;
    }
};

const visibilityFilter = (state = PersonFilterTypes.SHOW_ALL, action) => {
    const { type, payload } = action;
    switch (type) {
        case Consts.SET_VISIBILITY_FILTER:
            return payload;
        default:
            return state;
    }
};

const activePersonId = (state, action) => {
    const { type, payload } = action;
    switch (type) {
        case Consts.SET_ACTIVE_PERSON:
            return payload.id || 0; // if not id use 0;
        case Consts.CREATE_PERSON_SUCCESS:
            return payload.id;
        default:
            return state;
    }
};

const networkHandlingInitialState = {
    [CrudKeys.GET]: { status: RequestStatus.INITIAL },
    [CrudKeys.CREATE]: { status: RequestStatus.INITIAL },
    [CrudKeys.UPDATE]: { status: RequestStatus.INITIAL },
    [CrudKeys.DELETE]: { status: RequestStatus.INITIAL },
};

const networkHandling = (state = { ...networkHandlingInitialState }, action) => {
    const { type, payload, key } = action;
    const newState = { ...state };
    switch (type) {
        case Consts.GET_PEOPLE_REQUEST:
        case Consts.CREATE_PERSON_REQUEST:
        case Consts.UPDATE_PERSON_REQUEST:
            newState[key] = {
                status: RequestStatus.PENDING,
            };
            break;
        case Consts.GET_PEOPLE_SUCCESS:
        case Consts.CREATE_PERSON_SUCCESS:
        case Consts.UPDATE_PERSON_SUCCESS:
            newState[key] = {
                status: RequestStatus.SUCCESS,
            };
            break;
        case Consts.GET_PEOPLE_FAILURE:
        case Consts.CREATE_PERSON_FAILURE:
        case Consts.UPDATE_PERSON_FAILURE:
            newState[key] = {
                status: RequestStatus.FAILURE,
                error: payload,
            };
            break;
        default:
    }
    return newState;
};

const personReducer = (state = {}, action) => ({
    network: networkHandling(state.network, action),
    peopleById: peopleById(state.peopleById, action),
    peopleIds: peopleIds(state.peopleIds, action),
    visibilityFilter: visibilityFilter(state.visibilityFilter, action),
    activePersonId: activePersonId(state.activePersonId, action),
});

export default personReducer;
