import Notifications from 'react-notification-system-redux';

let notificationCounter = 0;
const defaultNotificationOpts = {
    title: 'Error',
    message: 'An error occured',
    position: 'tr',
    autoDismiss: 0,
};

const dispatchFetchErrorNotification = (response) => {
    notificationCounter += 1;
    return (dispatch) => {
        const notificationOpts = {
            ...defaultNotificationOpts,
            uid: notificationCounter,
            title: 'Network Error',
            message: `Error "${response.status}: ${response.statusText}" occured. Please try again later`,
        };
        dispatch(Notifications.error(notificationOpts));
    };
};

const dispatchErrorNotification = (error) => {
    notificationCounter += 1;
    return (dispatch) => {
        const notificationOpts = {
            ...defaultNotificationOpts,
            uid: notificationCounter,
            message: error.message,
        };
        dispatch(Notifications.error(notificationOpts));
    };
};

const dispatchSuccessNotification = (props) => {
    notificationCounter += 1;
    return (dispatch) => {
        const notificationOpts = {
            ...defaultNotificationOpts,
            ...props,
            uid: notificationCounter,
        };
        dispatch(Notifications.success(notificationOpts));
    };
};

const dispatchInfoNotification = (props) => {
    notificationCounter += 1;
    return (dispatch) => {
        const notificationOpts = {
            ...defaultNotificationOpts,
            ...props,
            uid: notificationCounter,
        };
        dispatch(Notifications.info(notificationOpts));
    };
};

const dispatchWarningNotification = (props) => {
    notificationCounter += 1;
    return (dispatch) => {
        const notificationOpts = {
            ...defaultNotificationOpts,
            ...props,
            uid: notificationCounter,
        };
        dispatch(Notifications.warning(notificationOpts));
    };
};

export {
    dispatchErrorNotification,
    dispatchFetchErrorNotification,
    dispatchSuccessNotification,
    dispatchInfoNotification,
    dispatchWarningNotification,
};
